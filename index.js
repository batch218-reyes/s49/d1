/*// mock database
let posts = [];

// post id
let count = 1;

*/


// the "fetch" method will return a 'promise' that relove to 'response' object 
fetch('https://jsonplaceholder.typicode.com/posts')
// the first ".then" method from the "Response" object to convert the data  retrieved into JSON fromat tro be used in ouyt aapplication
.then((response) => response.json())
// the second ".then" m,ethon prints the coonverted JSn value from the "fetchj request "
.then((data) => showPosts(data));


// Add post data----------------------------------------------------------------------
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	//prevents the page from reloading, 
	// prevents the default behavior of the event
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title'). value, 
			body: document.querySelector('#txt-body'). value,
			userId: 1
		}),
		// set the header data of the "request" objwect to sent to the backend 
		header: {'content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('successfully added!');

		document.querySelector("txt-title"). value = null; 
		document.querySelector("txt-body"). value = null;

	});
/*
	posts.push({
		// "id" - used for the unique ID for each posts
		id: count,
		// "title" and "value" will come from the "form-add-post"
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// increment the id value in count variable
	count++;

	console.log(posts);
	alert("Post successfully added!");

	showPosts(posts)

*/
});


// RETRIEVE POST DATA ----------------------------------------------------------------
const showPosts = (posts) => {
    // "postEntries" - variable that will contain all the posts
    let postEntries = "";

    posts.forEach((post) => {
        // "+=" - adds tha value of the right operand to a variable and assigns the result to the variable
        // "div" - division on where to place the value of title and posts
        postEntries += `
            <div id ="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });
    // To check what is stored in the properties of variables
    //console.log(postEntries);
    document.querySelector("#div-post-entries").innerHTML = postEntries;

};




// EDIT POST ---------------------------------------------------------------------------

const editPost = (id) => {
    
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
    // To remove the "disabled" attribute from the update button
    // removeAttributes() - removes the attribute with the specified name from the element
    document.querySelector('#btn-submit-update').removeAttribute('disabled');
};




// UPDATE POST 
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();
    
    // for (let i = 0; i < posts.length; i++) {

    //     if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
    //         posts[i].title = document.querySelector("#txt-edit-title").value;
    //         posts[i].body = document.querySelector("#txt-edit-body").value;

    //         showPosts(posts);
    //         alert("Successfully updated!");

    //         break;
    //     }
    // }

    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        // Sets the header data of the "request" object to be sent to the backend
        headers: {'Content-type': 'application/json; charset=UTF-8'}
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert('Successfully updated!')

    // Reset the edit post form input fields
    document.querySelector('#txt-edit-id').value = null;
    document.querySelector('#txt-edit-title').value = null;
    document.querySelector('#txt-edit-body').value = null;

    // .setAttribute - sets an attribute to an HTML element
    document.querySelector('#btn-submit-update').setAttribute('disabled', true);

    })
});




// DELETE POST------------------------------------------------------------------------------

const deletePost = (id) => {
/*    // Loops through all elements of the array to find the post ID that matches the post selected
    posts = posts.filter((post) => {
        if (post.id.toString() !== id) {
            return post;
        }
    });*/

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {method: 'DELETE'})
    // The Element.remove() method removes the element from the DOM.
    document.querySelector(`#post-${id}`).remove();
};
